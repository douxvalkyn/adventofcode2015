# ------------------
# ----- Day 08 -----
# ------------------
source("script.R")
source("outils.R")
data <- advent_input(8)
data <- data[-c(301)]

# ----- Part1 -----
sum(purrr::map_int(data, nchar)) -
sum(purrr::map_int(data, ~nchar(eval(parse(text = .x)), type = "bytes")))


