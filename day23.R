# ------------------
# ----- Day 23 -----
# ------------------
source("script.R")
source("outils.R")
data <- advent_input(23) |>  head( -1)

#data <- c("inc a","jio a, +2","tpl a","inc a")



#-----Part1-----
#algo obtenu par reverse engeneering
a=9663
b=0
go=TRUE
while (go==TRUE){
  if (a==1){
    print("OK")
    print(b)
    go=FALSE
  }else{
    b=b+1
    if (a%%2==0){
      a=a/2
    }else{
      a=a*3+1
    }
  }
}


#-----Part2-----
#algo obtenu par reverse engeneering
a=77671
b=0
go=TRUE
while (go==TRUE){
  if (a==1){
    print("OK")
    print(b)
    go=FALSE
  }else{
    b=b+1
    if (a%%2==0){
      a=a/2
    }else{
      a=a*3+1
    }
  }
}


#algo initial, trop long
a=0
b=0
i=1
while (i <= length(data)) {
  words <- strsplit(data[i], " ")
  commande = unlist(words)[1]
  register = unlist(words)[2]
  
  if (commande=="hlf"){
    assign(register, get(register)/2)
    i=i+1
  }
  if (commande=="tpl"){
    assign(register, get(register)*3)
    i=i+1
  }
  if (commande=="inc"){
    assign(register, get(register)+1)
    i=i+1
  }
  if (commande=="jmp"){
    offset=as.numeric(unlist(words)[2])
    i=i+offset
  }
  if (commande=="jie"){
    register=stringr::str_sub(register,1,1)
    if (get(register)%%2==0){
      offset=as.numeric(unlist(words)[3])
      i=i+offset
    }
  }
  if (commande=="jio"){
    register=stringr::str_sub(register,1,1)
    if (get(register)==1){
      offset=as.numeric(unlist(words)[3])
      i=i+offset
    }
  }
}